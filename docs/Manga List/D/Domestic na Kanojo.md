# Domestic na Kanojo / Love x Dilemma

![Domestic na Kanojo](https://myanimelist.cdn-dena.com/images/manga/2/196863l.jpg)

* Anglais : Domestic Girlfriend
* Titre alternatif : Dome×Kano / Love x Dilemma / Domestic Girlfriend
* Japonais : ドメスティックな彼女

## Informations

- Tomes VO : 21 (En cours)
- Tomes VF : 11 (En cours)

## Tomes

- [x] Domestic na Kanojo T1
- [x] Domestic na Kanojo T2
- [x] Domestic na Kanojo T3
- [x] Domestic na Kanojo T4
- [x] Domestic na Kanojo T5
- [x] Domestic na Kanojo T6
- [ ] Domestic na Kanojo T7
- [x] Domestic na Kanojo T8
- [x] Domestic na Kanojo T9
- [x] Domestic na Kanojo T10
- [x] Domestic na Kanojo T11
