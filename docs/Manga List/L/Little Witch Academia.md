# Little Witch Academia

![Little Witch Academia](https://myanimelist.cdn-dena.com/images/manga/1/196656l.jpg))

* Anglais : Little Witch Academia
* Japonais : リトルウィッチアカデミア

## Informations

- Tomes VO : 3 (Terminé)
- Tomes VF : 2 (En cours)

## Tomes

- [x] Little Witch Academia T1
- [x] Little Witch Academia T2
