# Wotaku ni Koi wa Muzukashii / Otaku Otaku

![Wotaku ni Koi wa Muzukashii](https://myanimelist.cdn-dena.com/images/manga/3/155740l.jpg)

* Anglais : Wotakoi: Love Is Hard for Otaku
* Titre alternatif : It's Difficult to Love an Otaku
* Japonais : ヲタクに恋は難しい

## Informations

- Tomes VO : 6 (En cours)
- Tomes VF : 4 (En cours)

## Tomes

- [x] Wotaku ni Koi wa Muzukashii T1
- [x] Wotaku ni Koi wa Muzukashii T2
- [ ] Wotaku ni Koi wa Muzukashii T3
- [ ] Wotaku ni Koi wa Muzukashii T4
