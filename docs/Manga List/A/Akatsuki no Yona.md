# Akatsuki no Yona / Yona, Princesse de l'Aube

![Akatsuki no Yona](https://myanimelist.cdn-dena.com/images/manga/3/153249l.jpg)

* Anglais : Yona of the Dawn
* Titre alternatif : Yona: The Girl Standing in the Blush of Dawn
* Japonais : 暁のヨナ

## Informations

- Tomes VO : 28 (En cours)
- Tomes VF : 26 (En cours)

## Tomes

- [x] Akatsuki no Yona T1
- [x] Akatsuki no Yona T2
- [ ] Akatsuki no Yona T3
- [ ] Akatsuki no Yona T4
- [ ] Akatsuki no Yona T5
- [ ] Akatsuki no Yona T6
- [ ] Akatsuki no Yona T7
- [ ] Akatsuki no Yona T8
- [ ] Akatsuki no Yona T9
- [ ] Akatsuki no Yona T10
- [ ] Akatsuki no Yona T11
- [ ] Akatsuki no Yona T12
- [ ] Akatsuki no Yona T13
- [ ] Akatsuki no Yona T14
- [ ] Akatsuki no Yona T15
- [ ] Akatsuki no Yona T16
- [ ] Akatsuki no Yona T17
- [ ] Akatsuki no Yona T18
- [ ] Akatsuki no Yona T19
- [ ] Akatsuki no Yona T20
- [ ] Akatsuki no Yona T21
- [ ] Akatsuki no Yona T22
- [x] Akatsuki no Yona T23
- [x] Akatsuki no Yona T24
- [ ] Akatsuki no Yona T25
- [ ] Akatsuki no Yona T26
