# Tayu Tayu

![Tayu Tayu](https://myanimelist.cdn-dena.com/images/manga/2/102791l.jpg)

* Anglais : Boing Boing
* Titre alternatif : Tayutayu
* Japonais : たゆたゆ

## Informations

- Tomes VO : 1 (Terminé)
- Tomes VF : 1 (Terminé)

## Tomes

- [x] Tayu Tayu T1
