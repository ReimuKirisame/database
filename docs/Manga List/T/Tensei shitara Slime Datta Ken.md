# Tensei shitara Slime Datta Ken / Moi, quand je me réincarne en Slime

![Tensei shitara Slime Datta Ken](https://myanimelist.cdn-dena.com/images/manga/3/167639l.jpg)

* Anglais : That Time I Got Reincarnated as a Slime
* Titre alternatif : Regarding Reincarnated to Slime / Re: My Reincarnation as a Slime
* Japonais : 転生したらスライムだった件

## Informations

- Tomes VO : 10 (En cours)
- Tomes VF : 6 (En cours)

## Tomes

- [x] Tensei shitara Slime Datta Ken T1
- [x] Tensei shitara Slime Datta Ken T2
- [x] Tensei shitara Slime Datta Ken T3
- [x] Tensei shitara Slime Datta Ken T4
- [x] Tensei shitara Slime Datta Ken T5
- [x] Tensei shitara Slime Datta Ken T6
