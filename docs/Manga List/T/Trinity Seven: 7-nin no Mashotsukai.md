# Trinity Seven: 7-nin no Mashotsukai / Trinity Seven

![Trinity Seven: 7-nin no Mashotsukai](https://myanimelist.cdn-dena.com/images/manga/1/63963l.jpg)

* Anglais : Trinity Seven: The Seven Magicians
* Titre alternatif : Trinity Seven: Shichinin no Mashotsukai / 7-nin no Mahoutsukai
* Japonais : トリニティセブン 7人の魔書使い

## Informations

- Tomes VO : 19 (En cours)
- Tomes VF : 14 (En cours)

## Tomes

- [x] Trinity Seven T1
- [ ] Trinity Seven T2
- [ ] Trinity Seven T3
- [ ] Trinity Seven T4
- [ ] Trinity Seven T5
- [ ] Trinity Seven T6
- [ ] Trinity Seven T7
- [ ] Trinity Seven T8
- [ ] Trinity Seven T9
- [x] Trinity Seven T10
- [ ] Trinity Seven T11
- [ ] Trinity Seven T12
- [ ] Trinity Seven T13
- [ ] Trinity Seven T14
