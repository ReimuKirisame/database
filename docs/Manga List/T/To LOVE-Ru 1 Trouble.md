# To LOVE-Ru Trouble

![To LOVE-Ru Trouble](https://myanimelist.cdn-dena.com/images/manga/3/63339l.jpg)

* Anglais : To Love Ru
* Titre alternatif : To LOVE-Ru Trouble / To-Love-Ru / To Love You / To Love You - Trouble / Toraburu / ToLoveRu
* Japonais: To LOVEる -とらぶる-

## Informations

- Tomes VO : 18 (Terminé)
- Tomes VF : 18 (Terminé)

## Tomes

- [x] To LOVE-Ru Trouble T1
- [x] To LOVE-Ru Trouble T2
- [x] To LOVE-Ru Trouble T3
- [x] To LOVE-Ru Trouble T4
- [x] To LOVE-Ru Trouble T5
- [x] To LOVE-Ru Trouble T6
- [x] To LOVE-Ru Trouble T7
- [x] To LOVE-Ru Trouble T8
- [x] To LOVE-Ru Trouble T9
- [x] To LOVE-Ru Trouble T10
- [x] To LOVE-Ru Trouble T11
- [x] To LOVE-Ru Trouble T12
- [x] To LOVE-Ru Trouble T13
- [x] To LOVE-Ru Trouble T14
- [x] To LOVE-Ru Trouble T15
- [x] To LOVE-Ru Trouble T16
- [x] To LOVE-Ru Trouble T17
- [x] To LOVE-Ru Trouble T18
