# Tales of Xillia: Side;Milla

![Tales of Xillia: Side;Milla](https://myanimelist.cdn-dena.com/images/manga/2/121371l.jpg)

* Japonais : テイルズ オブ エクシリア SIDE;MILLA

## Informations

- Tomes VO : 5 (Terminé)
- Tomes VF : 5 (Terminé)

## Tomes

- [x] Tales of Xillia: Side;Milla T1
- [x] Tales of Xillia: Side;Milla T2
- [x] Tales of Xillia: Side;Milla T3
- [x] Tales of Xillia: Side;Milla T4
- [x] Tales of Xillia: Side;Milla T5
