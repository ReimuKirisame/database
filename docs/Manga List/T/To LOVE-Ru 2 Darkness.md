# To LOVE-Ru Darkness

![To LOVE-Ru Darkness](https://myanimelist.cdn-dena.com/images/manga/3/109331l.jpg)

* Anglais : To Love Ru: Darkness
* Titre alternatif : Trouble: Darkness
* Japonais : To LOVEる -とらぶる- ダークネス

## Informations

- Tomes VO : 18 (Terminé)
- Tomes VF : 18 (Terminé)

## Tomes

- [x] To LOVE-Ru Darkness T1
- [x] To LOVE-Ru Darkness T2
- [x] To LOVE-Ru Darkness T3
- [x] To LOVE-Ru Darkness T4
- [x] To LOVE-Ru Darkness T5
- [x] To LOVE-Ru Darkness T6
- [x] To LOVE-Ru Darkness T7
- [x] To LOVE-Ru Darkness T8
- [x] To LOVE-Ru Darkness T9
- [x] To LOVE-Ru Darkness T10
- [x] To LOVE-Ru Darkness T11
- [x] To LOVE-Ru Darkness T12
- [x] To LOVE-Ru Darkness T13
- [x] To LOVE-Ru Darkness T14
- [x] To LOVE-Ru Darkness T15
- [x] To LOVE-Ru Darkness T16
- [x] To LOVE-Ru Darkness T17
- [x] To LOVE-Ru Darkness T18
