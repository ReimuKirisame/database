# Tales of Berseria

![Tales of Berseria](https://myanimelist.cdn-dena.com/images/manga/1/195540l.jpg)

* Japonais : テイルズ オブ ベルセリア

## Informations

- Tomes VO : 3 (Terminé)
- Tomes VF : 3 (Terminé)

## Tomes

- [x] Tales of Berseria T1
- [x] Tales of Berseria T2
- [ ] Tales of Berseria T3
