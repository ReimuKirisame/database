# Tate no Yuusha no Nariagari / The Rising of the Shield Hero

![Tate no Yuusha no Nariagari](https://myanimelist.cdn-dena.com/images/manga/2/130759l.jpg)

* Anglais : ERASED
* Titre alternatif : The Rising of the Shield Hero: The Manga Companion
* Japonais : 盾の勇者の成り上がり

## Informations

- Tomes VO : 12 (En cours)
- Tomes VF : 11 (En cours)

## Tomes

- [x] Tate no Yuusha no Nariagari T1
- [x] Tate no Yuusha no Nariagari T2
- [x] Tate no Yuusha no Nariagari T3
- [x] Tate no Yuusha no Nariagari T4
- [ ] Tate no Yuusha no Nariagari T5
- [ ] Tate no Yuusha no Nariagari T6
- [ ] Tate no Yuusha no Nariagari T7
- [ ] Tate no Yuusha no Nariagari T8
- [ ] Tate no Yuusha no Nariagari T9
- [ ] Tate no Yuusha no Nariagari T10
- [ ] Tate no Yuusha no Nariagari T11
