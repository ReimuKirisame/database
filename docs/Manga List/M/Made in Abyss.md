# Made in Abyss

![Made in Abyss](https://myanimelist.cdn-dena.com/images/manga/3/161645l.jpg)

* Anglais : Made in Abyss
* Japonais : メイドインアビス

## Informations

- Tomes VO : 7 (En cours)
- Tomes VF : 4 (En cours)

## Tomes

- [x] Made in Abyss T1
- [ ] Made in Abyss T2
- [ ] Made in Abyss T3
- [ ] Made in Abyss T4
