# Noragami

![Noragami](https://myanimelist.cdn-dena.com/images/manga/2/90899l.jpg)

* Anglais : Noragami: Stray God
* Titre alternatif : Stray Gods
* Japonais : ノラガミ

## Informations

- Tomes VO : 20 (En cours)
- Tomes VF : 19 (En cours)

## Tomes

- [x] Noragami T1
- [x] Noragami T2
- [ ] Noragami T3
- [ ] Noragami T4
- [ ] Noragami T5
- [x] Noragami T6
- [x] Noragami T7
- [ ] Noragami T8
- [ ] Noragami T9
- [ ] Noragami T10
- [ ] Noragami T11
- [ ] Noragami T12
- [ ] Noragami T13
- [ ] Noragami T14
- [ ] Noragami T15
- [ ] Noragami T16
- [ ] Noragami T17
- [ ] Noragami T18
- [ ] Noragami T19
