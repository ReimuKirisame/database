# Nanatsu no Taizai: Seven Days / Seven Deadly Sins Seven Days

![Nanatsu no Taizai: Seven Days](https://myanimelist.cdn-dena.com/images/manga/2/189153l.jpg)

* Anglais : The Seven Deadly Sins: Seven Days
* Japonais : 七つの大罪 セブンデイズ

## Informations

- Tomes VO : 2 (Terminé)
- Tomes VF : 2 (En cours)

## Tomes

- [x] Nanatsu no Taizai: Seven Days T1
- [x] Nanatsu no Taizai: Seven Days T2
