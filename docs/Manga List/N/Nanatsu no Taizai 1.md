# Nanatsu no Taizai / Seven Deadly Sins

![Nanatsu no Taizai](https://myanimelist.cdn-dena.com/images/manga/2/153111l.jpg)

* Anglais : The Seven Deadly Sins
* Japonais : 七つの大罪

## Informations

- Tomes VO : 35 (En cours)
- Tomes VF : 31 (En cours)

## Tomes

- [x] Nanatsu no Taizai T1
- [ ] Nanatsu no Taizai T2
- [ ] Nanatsu no Taizai T3
- [ ] Nanatsu no Taizai T4
- [ ] Nanatsu no Taizai T5
- [ ] Nanatsu no Taizai T6
- [x] Nanatsu no Taizai T7
- [x] Nanatsu no Taizai T8
- [ ] Nanatsu no Taizai T9
- [ ] Nanatsu no Taizai T10
- [x] Nanatsu no Taizai T11
- [x] Nanatsu no Taizai T12
- [x] Nanatsu no Taizai T13
- [x] Nanatsu no Taizai T14
- [x] Nanatsu no Taizai T15
- [x] Nanatsu no Taizai T16
- [ ] Nanatsu no Taizai T17
- [ ] Nanatsu no Taizai T18
- [ ] Nanatsu no Taizai T19
- [ ] Nanatsu no Taizai T20
- [ ] Nanatsu no Taizai T21
- [ ] Nanatsu no Taizai T22
- [x] Nanatsu no Taizai T23
- [x] Nanatsu no Taizai T24
- [x] Nanatsu no Taizai T25
- [x] Nanatsu no Taizai T26
- [x] Nanatsu no Taizai T27
- [x] Nanatsu no Taizai T28
- [x] Nanatsu no Taizai T29
- [x] Nanatsu no Taizai T30
- [x] Nanatsu no Taizai T31
