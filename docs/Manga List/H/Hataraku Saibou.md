# Hataraku Saibou / Les Brigades Immunitaires

![Hataraku Saibou](https://myanimelist.cdn-dena.com/images/manga/3/200497l.jpg)

* Anglais : Cell's at Work!
* Japonais : はたらく細胞

## Informations

- Tomes VO : 5 (En cours)
- Tomes VF : 5 (En cours)

## Tomes

- [x] Hataraku Saibou T1
- [x] Hataraku Saibou T2
- [ ] Hataraku Saibou T3
- [ ] Hataraku Saibou T4
- [ ] Hataraku Saibou T5
