# Henshin / Métamorphose

![Henshin](https://myanimelist.cdn-dena.com/images/manga/2/178602l.jpg)

* Anglais : Metamorphosis
* Titre alternatif : Emergence.
* Japonais : 変身

## Informations

- Tomes VO : 1 (Terminé)
- Tomes VF : 1 (Terminé)

## Tomes

- [x] Henshin T1
