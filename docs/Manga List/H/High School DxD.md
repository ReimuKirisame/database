# High School DxD

![High School DxD](https://myanimelist.cdn-dena.com/images/manga/1/206676l.jpg)

* Titre alternatif : Highschool DxD
* Japonais : ハイスクールD×D

## Informations

- Tomes VO : 11 (Terminé)
- Tomes VF : 11 (Terminé)

## Tomes

- [x] High School DxD T1
- [x] High School DxD T2
- [x] High School DxD T3
- [x] High School DxD T4
- [x] High School DxD T5
- [x] High School DxD T6
- [x] High School DxD T7
- [x] High School DxD T8
- [x] High School DxD T9
- [x] High School DxD T10
- [x] High School DxD T11
- [x] High School DxD SPIN OFF 1
- [x] High School DxD SPIN OFF 2
