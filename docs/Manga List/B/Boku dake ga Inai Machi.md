# Boku dake ga Inai Machi / ERASED

![Boku dake ga Inai Machi](https://myanimelist.cdn-dena.com/images/manga/1/172082l.jpg)

* Anglais : ERASED
* Titre alternatif : The Town Where Only I am Missing / BokuMachi
* Japonais : 僕だけがいない街

## Informations

- Tomes VO : 9 (Terminé)
- Tomes VF : 9 (Terminé)

## Tomes

- [x] Boku dake ga Inai Machi T1
- [x] Boku dake ga Inai Machi T2
- [ ] Boku dake ga Inai Machi T3
- [x] Boku dake ga Inai Machi T4
- [x] Boku dake ga Inai Machi T5
- [x] Boku dake ga Inai Machi T6
- [ ] Boku dake ga Inai Machi T7
- [ ] Boku dake ga Inai Machi T8
- [ ] Boku dake ga Inai Machi T9
