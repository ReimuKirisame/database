# Manga Database

## Changelog

### 27/02/2019 - 0.1.3
* Changement du terme "Volume" en "Tome".

### 27/02/2019 - 0.1.2
* Ajout de mangas et corrections d'erreurs.

### 27/02/2019 - 0.1.1
* Ajout de mangas.

### 26/02/2019 - 0.1.0
* Création du projet.
