# Kakegurui Twin / GAMBLING SCHOOL TWIN

![Kakegurui Twin](https://myanimelist.cdn-dena.com/images/manga/2/169926l.jpg)

* Titre alternatif : Compulsive Gambler Twin
* Japonais : ケグルイ双〈ツイン〉

## Informations

- Tomes VO : 7 (En cours)
- Tomes VF : 3 (En cours)

## Tomes

- [x] Kakegurui Twin T1
- [x] Kakegurui Twin T2
- [ ] Kakegurui Twin T3
