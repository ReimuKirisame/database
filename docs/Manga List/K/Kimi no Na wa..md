# Kimi no Na wa. / Your name.

![Kimi no Na wa.](https://myanimelist.cdn-dena.com/images/manga/1/182270l.jpg)

* Anglais : Your name.
* Japonais : 君の名は。

## Informations

- Tomes VO : 3 (Terminé)
- Tomes VF : 3 (Terminé)

## Tomes

- [x] Kimi no Na wa. T1
- [ ] Kimi no Na wa. T2
- [ ] Kimi no Na wa. T3
