# Koe no Katachi / Silent voice

![Koe no Katachi](https://myanimelist.cdn-dena.com/images/manga/1/120529l.jpg)

* Titre alternatif : The Shape of Voice
* Japonais : 聲の形

## Informations

- Tomes VO : 7 (Terminé)
- Tomes VF : 7 (Terminé)

## Tomes

- [x] Koe no Katachi T1
- [x] Koe no Katachi T2
- [ ] Koe no Katachi T3
- [ ] Koe no Katachi T4
- [ ] Koe no Katachi T5
- [ ] Koe no Katachi T6
- [ ] Koe no Katachi T7
