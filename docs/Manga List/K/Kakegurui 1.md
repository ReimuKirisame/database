# Kakegurui / GAMBLING SCHOOL

![Kakegurui](https://myanimelist.cdn-dena.com/images/manga/1/142175l.jpg)

* Japonais : 賭ケグルイ

## Informations

- Tomes VO : 10 (En cours)
- Tomes VF : 8 (En cours)

## Tomes

- [x] Kakegurui T1
- [x] Kakegurui T2
- [x] Kakegurui T3
- [x] Kakegurui T4
- [x] Kakegurui T5
- [x] Kakegurui T6
- [ ] Kakegurui T7
- [ ] Kakegurui T8
