# Vanilla Essence

![Vanilla Essence](https://myanimelist.cdn-dena.com/images/manga/2/164997l.jpg)

* Titre alternatif : Ikenai Asobi / Maru Game / Nanido Settei Very Easy♥ / Tadaima Pool Seisouchu / Trip On Trip / Touch Me if You Can! / Hatsumoude / You Gotta Star / Hayaku Dashite ne / Hayaku Teishutsu shite ne / Naughty Game
* Japonais : ヴァニラエッセンス

## Informations

- Tomes VO : 1 (Terminé)
- Tomes VF : 1 (Terminé)

## Tomes

- [x] Vanilla Essence T1
