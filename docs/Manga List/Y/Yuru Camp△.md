# Yuru Camp△

![Yuru Camp△](https://myanimelist.cdn-dena.com/images/manga/1/168083l.jpg)

* Titre alternatif : Yurukyan
* Japonais : ゆるキャン△

## Information

- Tomes VO : 7 (En cours)
- Tomes VF : 5 (En cours)

## Tomes

- [x] Yuru Camp△ T1
- [x] Yuru Camp△ T2
- [x] Yuru Camp△ T3
- [x] Yuru Camp△ T4
- [x] Yuru Camp△ T5
