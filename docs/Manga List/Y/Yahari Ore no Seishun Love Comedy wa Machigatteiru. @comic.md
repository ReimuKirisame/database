# Yahari Ore no Seishun Love Comedy wa Machigatteiru. @comic / My Teen Romantic Comedy is Wrong as I Expected

![Yahari Ore no Seishun Love Comedy wa Machigatteiru. @comic](https://myanimelist.cdn-dena.com/images/manga/3/138503l.jpg)

* Anglais : My Youth Romantic Comedy Is Wrong, As I Expected @comic
* Japonais : やはり俺の青春ラブコメはまちがっている。@comic

## Informations

- Tomes VO : 11 (En cours)
- Tomes VF : 3 (En cours)

## Tomes

- [x] Yahari Ore no Seishun Love Comedy wa Machigatteiru. @comic T1
- [x] Yahari Ore no Seishun Love Comedy wa Machigatteiru. @comic T2
- [x] Yahari Ore no Seishun Love Comedy wa Machigatteiru. @comic T3
