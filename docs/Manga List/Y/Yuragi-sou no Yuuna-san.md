# Yuragi-sou no Yuuna-san / Yûna de la pension Yuragi

![Yuragi-sou no Yuuna-san](https://myanimelist.cdn-dena.com/images/manga/3/179022l.jpg)

* Anglais : Yuuna and the Haunted Hot Springs
* Titre alternatif : Yuuna of Yuragi Manor
* Japonais : ゆらぎ荘の幽奈さん

## Informations

- Tomes VO : 14 (En cours)
- Tomes VF : 7 (En cours)

## Tomes

- [x] Yuragi-sou no Yuuna-san T1
- [x] Yuragi-sou no Yuuna-san T2
- [x] Yuragi-sou no Yuuna-san T3
- [x] Yuragi-sou no Yuuna-san T4
- [x] Yuragi-sou no Yuuna-san T5
- [x] Yuragi-sou no Yuuna-san T6
- [x] Yuragi-sou no Yuuna-san T7
