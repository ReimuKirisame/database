# Shokugeki no Souma

![Shokugeki no Souma](https://myanimelist.cdn-dena.com/images/manga/1/115803l.jpg)

* Anglais : Food Wars: Shokugeki no Soma
* Titre alternatif : Shokugeki no Soma / Food Wars: Shokugeki no Soma
* Japonais : 食戟のソーマ

## Informations

- Tomes VO : 33 (En cours)
- Tomes VF : 27 (En cours)

## Tomes

- [x] Shokugeki no Souma T1
- [x] Shokugeki no Souma T2
- [x] Shokugeki no Souma T3
- [ ] Shokugeki no Souma T4
- [x] Shokugeki no Souma T5
- [ ] Shokugeki no Souma T6
- [x] Shokugeki no Souma T7
- [ ] Shokugeki no Souma T8
- [ ] Shokugeki no Souma T9
- [x] Shokugeki no Souma T10
- [ ] Shokugeki no Souma T11
- [ ] Shokugeki no Souma T12
- [ ] Shokugeki no Souma T13
- [ ] Shokugeki no Souma T14
- [ ] Shokugeki no Souma T15
- [x] Shokugeki no Souma T16
- [ ] Shokugeki no Souma T17
- [x] Shokugeki no Souma T18
- [x] Shokugeki no Souma T19
- [x] Shokugeki no Souma T20
- [x] Shokugeki no Souma T21
- [ ] Shokugeki no Souma T22
- [ ] Shokugeki no Souma T23
- [ ] Shokugeki no Souma T24
- [ ] Shokugeki no Souma T25
- [ ] Shokugeki no Souma T26
- [ ] Shokugeki no Souma T27
