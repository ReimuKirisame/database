# Shingeki no Kyojin / L'attaque des titans

![Shingeki no Kyojin](https://myanimelist.cdn-dena.com/images/manga/2/37846l.jpg))

* Anglais : Attack on Titan
* Japonais : 進撃の巨人

## Informations

- Tomes VO : 27 (En cours)
- Tomes VF : 26 (En cours)

## Tomes

- [x] Shingeki no Kyojin T1
- [x] Shingeki no Kyojin T2
- [ ] Shingeki no Kyojin T3
- [ ] Shingeki no Kyojin T4
- [ ] Shingeki no Kyojin T5
- [ ] Shingeki no Kyojin T6
- [ ] Shingeki no Kyojin T7
- [ ] Shingeki no Kyojin T8
- [ ] Shingeki no Kyojin T9
- [ ] Shingeki no Kyojin T10
- [ ] Shingeki no Kyojin T11
- [ ] Shingeki no Kyojin T12
- [ ] Shingeki no Kyojin T13
- [ ] Shingeki no Kyojin T14
- [ ] Shingeki no Kyojin T15
- [ ] Shingeki no Kyojin T16
- [ ] Shingeki no Kyojin T17
- [ ] Shingeki no Kyojin T18
- [ ] Shingeki no Kyojin T19
- [ ] Shingeki no Kyojin T20
- [ ] Shingeki no Kyojin T21
- [ ] Shingeki no Kyojin T22
- [ ] Shingeki no Kyojin T23
- [ ] Shingeki no Kyojin T24
- [ ] Shingeki no Kyojin T25
- [ ] Shingeki no Kyojin T26
