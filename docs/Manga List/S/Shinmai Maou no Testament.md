# Shinmai Maou no Testament / The Testament of Sister New Devil

![Shinmai Maou no Testament](https://myanimelist.cdn-dena.com/images/manga/1/105683l.jpg)

* Titre alternatif : Shinmai Maou no Keiyakusha, The Testament of Sister New Devil
* Japonais : 新妹魔王の契約者〈テスタメント〉

## Informations

- Tomes VO : 9 (Terminé)
- Tomes VF : 9 (Terminé)

## Tomes

- [x] Shinmai Maou no Testament T1
- [x] Shinmai Maou no Testament T2
- [x] Shinmai Maou no Testament T3
- [x] Shinmai Maou no Testament T4
- [x] Shinmai Maou no Testament T5
- [x] Shinmai Maou no Testament T6
- [x] Shinmai Maou no Testament T7
- [x] Shinmai Maou no Testament T8
- [x] Shinmai Maou no Testament T9
