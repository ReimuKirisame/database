# Shinmai Maou no Testament: Arashi / The Testament of Sister New Devil STORM

![Shinmai Maou no Testament: Arashi](https://myanimelist.cdn-dena.com/images/manga/1/147721l.jpg)

* Titre alternatif : Shinmai Maou no Keiyakusha Arashi
* Japonais : 新妹魔王の契約者〈テスタメント〉・嵐！

## Informations

- Tomes VO : 5 (Terminé)
- Tomes VF : 5 (Terminé)

## Tomes

- [x] Shinmai Maou no Testament: Arashi T1
- [x] Shinmai Maou no Testament: Arashi T2
- [x] Shinmai Maou no Testament: Arashi T3
- [x] Shinmai Maou no Testament: Arashi T4
- [x] Shinmai Maou no Testament: Arashi T5
