# Shigatsu wa Kimi no Uso / Your Lie in April

![Shigatsu wa Kimi no Uso](https://myanimelist.cdn-dena.com/images/manga/3/102691l.jpg)

* Anglais : Your Lie in April
* Japonais : 四月は君の嘘

## Informations

- Tomes VO : 11 (Terminé)
- Tomes VF : 11 (Terminé)

## Tomes

- [ ] Shigatsu wa Kimi no Uso T1
- [ ] Shigatsu wa Kimi no Uso T2
- [ ] Shigatsu wa Kimi no Uso T3
- [ ] Shigatsu wa Kimi no Uso T4
- [ ] Shigatsu wa Kimi no Uso T5
- [ ] Shigatsu wa Kimi no Uso T6
- [ ] Shigatsu wa Kimi no Uso T7
- [x] Shigatsu wa Kimi no Uso T8
- [x] Shigatsu wa Kimi no Uso T9
- [ ] Shigatsu wa Kimi no Uso T10
- [ ] Shigatsu wa Kimi no Uso T11
- [x] Shigatsu wa Kimi no Uso CODA
