# Citrus

![Citrus](https://myanimelist.cdn-dena.com/images/manga/3/185905l.jpg)

* Anglais : Citrus
* Japonais : citrus

## Informations

 - Tomes VO : 10 (Terminé)
 - Tomes VF : 8 (En cours)

## Tomes

 - [x] Citrus T1
 - [x] Citrus T2
 - [x] Citrus T3
 - [x] Citrus T4
 - [x] Citrus T5
 - [x] Citrus T6
 - [x] Citrus T7
 - [x] Citrus T8
