# Fuuka / Fûka

![Fuuka](https://myanimelist.cdn-dena.com/images/manga/2/186209l.jpg)

* Anglais : Fuuka
* Japonais : 風夏

## Informations

- Tomes VO : 10 (Terminé)
- Tomes VF : 12 (En cours)

## Tomes

- [x] Fuuka T1
- [x] Fuuka T2
- [x] Fuuka T3
- [x] Fuuka T4
- [x] Fuuka T5
- [x] Fuuka T6
- [x] Fuuka T7
- [ ] Fuuka T8
- [ ] Fuuka T9
- [ ] Fuuka T10
- [ ] Fuuka T11
- [ ] Fuuka T12
