# Fairy Tail

![Fairy Tail](https://myanimelist.cdn-dena.com/images/manga/3/198604l.jpg)

* Anglais : Fairy Tail
* Japonais : FAIRY TAIL

## Informations

- Tomes VO : 63 (Terminé)
- Tomes VF : 63 (Terminé)

## Tomes

- [x] Fairy Tail T1
- [x] Fairy Tail T2
- [x] Fairy Tail T3
- [x] Fairy Tail T4
- [x] Fairy Tail T5
- [x] Fairy Tail T6
- [x] Fairy Tail T7
- [x] Fairy Tail T8
- [x] Fairy Tail T9
- [x] Fairy Tail T10
- [x] Fairy Tail T11
- [x] Fairy Tail T12
- [x] Fairy Tail T13
- [x] Fairy Tail T14
- [x] Fairy Tail T15
- [x] Fairy Tail T16
- [x] Fairy Tail T17
- [x] Fairy Tail T18
- [x] Fairy Tail T19
- [x] Fairy Tail T20
- [x] Fairy Tail T21
- [x] Fairy Tail T22
- [x] Fairy Tail T23
- [x] Fairy Tail T24
- [x] Fairy Tail T25
- [x] Fairy Tail T26
- [x] Fairy Tail T27
- [x] Fairy Tail T28
- [x] Fairy Tail T29
- [x] Fairy Tail T30
- [x] Fairy Tail T31
- [x] Fairy Tail T32
- [x] Fairy Tail T33
- [x] Fairy Tail T34
- [x] Fairy Tail T35
- [x] Fairy Tail T36
- [x] Fairy Tail T37
- [x] Fairy Tail T38
- [x] Fairy Tail T39
- [x] Fairy Tail T40
- [x] Fairy Tail T41
- [x] Fairy Tail T42
- [x] Fairy Tail T43
- [x] Fairy Tail T44
- [x] Fairy Tail T45
- [x] Fairy Tail T46
- [x] Fairy Tail T47
- [x] Fairy Tail T48
- [x] Fairy Tail T49
- [x] Fairy Tail T50
- [x] Fairy Tail T51
- [x] Fairy Tail T52
- [x] Fairy Tail T53
- [x] Fairy Tail T54
- [x] Fairy Tail T55
- [x] Fairy Tail T56
- [x] Fairy Tail T57
- [x] Fairy Tail T58
- [x] Fairy Tail T59
- [x] Fairy Tail T60
- [x] Fairy Tail T61
- [x] Fairy Tail T62
- [x] Fairy Tail T63
