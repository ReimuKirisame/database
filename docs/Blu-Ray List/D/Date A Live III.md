# Date A Live III / デート・ア・ライブIII

![Date A Live III](https://myanimelist.cdn-dena.com/images/anime/1295/97615l.jpg)

## Informations

- Site web officiel : http://date-a-live-anime.com/
- Total : 2 BDBOX.

## Possédés

- [ ] Date A Live III BOX.1
- [ ] Date A Live III BOX.2

## Précommandes

- [x] Date A Live III BOX.1
- [x] Date A Live III BOX.2
