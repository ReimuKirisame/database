# Domestic na Kanojo / ドメスティックな彼女

![Domestic na Kanojo!](https://myanimelist.cdn-dena.com/images/anime/1021/95670l.jpg)

## Informations

- Site web officiel : http://domekano-anime.com/
- En attente des annonces officielles.
- OVA compris dans l'édition limitée du tome 22.

## Possédés

- [ ] OVA compris dans l'édition limitée du tome 22

## Précommandes

- [x] OVA compris dans l'édition limitée du tome 22
