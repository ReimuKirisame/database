# Gotoubun no Hanayome / 五等分の花嫁

![Gotoubun no Hanayome](https://myanimelist.cdn-dena.com/images/anime/1819/97947l.jpg)

## Informations

- Site web officiel : http://www.tbs.co.jp/anime/5hanayome/
- Total : 5 Volumes

## Possédés

- [ ] Gotoubun no Hanayome Vol.1
- [ ] Gotoubun no Hanayome Vol.2
- [ ] Gotoubun no Hanayome Vol.3
- [ ] Gotoubun no Hanayome Vol.4
- [ ] Gotoubun no Hanayome Vol.5 Fin

## Précommandes

- [x] Gotoubun no Hanayome Vol.1
- [x] Gotoubun no Hanayome Vol.2
- [x] Gotoubun no Hanayome Vol.3
- [x] Gotoubun no Hanayome Vol.4
- [x] Gotoubun no Hanayome Vol.5 Fin
