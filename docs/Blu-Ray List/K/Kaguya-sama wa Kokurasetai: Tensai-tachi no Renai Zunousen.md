# Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen / かぐや様は告らせたい～天才たちの恋愛頭脳戦～

![Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen](https://myanimelist.cdn-dena.com/images/anime/1291/97023l.jpg)

## Informations

- Site web officiel : https://kaguya.love/
- Total : 6 Volumes

## Possédés

- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.1
- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.2
- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.3
- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.4
- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.5
- [ ] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.6 Fin

## Précommandes

- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.1
- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.2
- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.3
- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.4
- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.5
- [x] Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen Vol.6 Fin
