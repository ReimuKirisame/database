# Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai / 青春ブタ野郎はバニーガール先輩の夢を見ない

![Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai](https://myanimelist.cdn-dena.com/images/anime/1301/93586l.jpg)

## Informations

- Site web officiel : https://ao-buta.com/
- Total : 5 Volumes

## Possédés

- [x] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.1
- [x] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.2
- [ ] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.3
- [ ] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.4
- [ ] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.5 Fin

## Précommandes

- [x] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.3
- [x] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.4
- [x] Seishun Buta Yarou wa Bunny Girl Senpai no Yume wo Minai Vol.5 Fin
