# Sunohara-sou no Kanrinin-san / すのはら荘の管理人さん

![Sunohara-sou no Kanrinin-san](https://myanimelist.cdn-dena.com/images/anime/1368/95074l.jpg)

## Informations

- Site web officiel : http://sunoharasou-anime.com/
- Total : 4 Volumes

## Possédés

- [x] Sunohara-sou no Kanrinin-san Vol.1
- [x] Sunohara-sou no Kanrinin-san Vol.2
- [x] Sunohara-sou no Kanrinin-san Vol.3
- [x] Sunohara-sou no Kanrinin-san Vol.4 Fin
