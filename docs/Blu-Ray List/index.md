# Blu-Ray Database

## Changelog

### 27/02/2019 - 0.1.2
* Ajout des sites web officiels et corrections.

### 27/02/2019 - 0.1.1
* Ajout de Blu-Rays.

### 27/02/2019 - 0.1.0
* Création du projet.
